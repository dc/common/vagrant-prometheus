#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"
# because script is located in scripts
project_base=$DIR/..
openssl_base=$project_base/openssl
config=$openssl_base/config
###################################################################################################

pushd $openssl_base > /dev/null 2>&1

if [ -d $openssl_base/data ];then
	echo -n "Openssl-data directory already exist. Create new certificates ? [y/n]: " 
	read prompt
	if ! [ "$prompt" != "${prompt#[Yy]}" ] ;then
		echo "Abort."
		exit
	fi
fi 

# Create data directory
rm -r data > /dev/null 2>&1
mkdir data > /dev/null 2>&1
pushd data > /dev/null 2>&1

# Certificate folder
mkdir certificates > /dev/null 2>&1
# Cert sign request database folder
mkdir csrdb > /dev/null 2>&1

# Create text files for database
touch csrdb/index.txt
touch csrdb/serial.txt
if [ "$1" == "" ];then
    echo "02" > csrdb/serial.txt
else
    echo "$1" > csrdb/serial.txt
fi 


# Create CA-Certificate 
yes "" | openssl req -x509 -config $config/openssl-ca.cnf -newkey rsa:4096 -sha1 -nodes -out certificates/cacert.pem -keyout certificates/cakey.pem -outform PEM -days 3650 > /dev/null 2>&1
echo "CA-Certificate created. $(pwd)/certificates/cacert.pem"

# Create CSR for influx, grafana, chronograf
yes "" | openssl req -config $config/influx.cnf -newkey rsa:4096 -sha1 -nodes -out influx.csr -keyout certificates/influxkey.pem -outform PEM > /dev/null 2>&1
yes "" | openssl req -config $config/grafana.cnf -newkey rsa:4096 -sha1 -nodes -out grafana.csr -keyout certificates/grafanakey.pem -outform PEM > /dev/null 2>&1
yes "" | openssl req -config $config/chronograf.cnf -newkey rsa:4096 -sha1 -nodes -out chronograf.csr -keyout certificates/chronografkey.pem -outform PEM > /dev/null 2>&1

# Switch to database folder to sign
pushd csrdb/ > /dev/null 2>&1

# Sign CSR influx, grafana, chronograf
openssl ca -config $config/openssl-ca.cnf -policy signing_policy -extensions signing_req -batch -cert ../certificates/cacert.pem -keyfile ../certificates/cakey.pem  -out ../certificates/influxcert.pem -infiles ../influx.csr > /dev/null 2>&1
openssl ca -config $config/openssl-ca.cnf -policy signing_policy -extensions signing_req -batch -cert ../certificates/cacert.pem -keyfile ../certificates/cakey.pem  -out ../certificates/grafanacert.pem -infiles ../grafana.csr > /dev/null 2>&1
openssl ca -config $config/openssl-ca.cnf -policy signing_policy -extensions signing_req -batch -cert ../certificates/cacert.pem -keyfile ../certificates/cakey.pem  -out ../certificates/chronografcert.pem -infiles ../chronograf.csr > /dev/null 2>&1
echo "Certificates signed and created. $(pwd)/certificates/"

# Remove requests
popd > /dev/null 2>&1
rm *.csr
pushd certificates > /dev/null 2>&1


# Link files
provision=$project_base/provision

# prom CA
deploy=$provision/prometheus/https
rm -r -f $deploy
mkdir $deploy
ln cacert.pem $deploy/cacert.pem

# graf CA, cert, key
deploy=$provision/grafana/https
rm -r -f $deploy
mkdir $deploy
ln cacert.pem $deploy/cacert.pem
ln grafanacert.pem $deploy/grafanacert.pem
ln grafanakey.pem $deploy/grafanakey.pem

# flux CA, cert, key
deploy=$provision/influx/https
rm -r -f $deploy
mkdir $deploy
ln cacert.pem $deploy/cacert.pem
ln influxcert.pem $deploy/influxcert.pem
ln influxkey.pem $deploy/influxkey.pem

# chron CA, cert, key
deploy=$provision/chronograf/https
rm -r -f $deploy
mkdir $deploy
ln cacert.pem $deploy/cacert.pem
ln chronografcert.pem $deploy/chronografcert.pem
ln chronografkey.pem $deploy/chronografkey.pem
echo "Certificates and keys are linked to to their provision folders. "