#!/bin/bash


DIR="$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1
CERT_FOLDER=$DIR/../openssl/data/certificates
echo "$CERT_FOLDER"

if ! [ -d $CERT_FOLDER ];then
  echo "No certificates created. "
  exit
fi

pushd $CERT_FOLDER > /dev/null
for cert in ./*cert*
do
  echo "File: $cert"
  echo "---"
  openssl x509 -noout -fingerprint -sha256 -inform pem -in $cert
  echo ""
  openssl x509 -noout -fingerprint -sha1 -inform pem -in $cert
  echo "---"
  echo ""
  echo ""
done