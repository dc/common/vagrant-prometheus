#!/bin/bash




DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"
# because script is located in scripts
project_base=$DIR/..
config=$project_base/openssl/config
certificates=$project_base/openssl/data/certificates
csrdb=$project_base/openssl/data/csrdb

# check if cakey.pem exist, if not abort 
if ! [ -f "$project_base/openssl/data/certificates/cakey.pem" ];then
  echo "No cakey.pem found, please first run \"./scripts/openssl_generate.sh\""
  exit
fi

pushd $csrdb > /dev/null

case $1 in
  "graf")
  deploy=$project_base/provision/grafana/https
  #remove old
  rm -f $certificates/grafanacert.pem
  rm -f $certificates/grafanakey.pem
  rm -f $deploy/grafanacert.pem
  rm -f $deploy/grafanakey.pem
  
  #generate new
  yes "" | openssl req -config $config/grafana.cnf -newkey rsa:4096 -sha1 -nodes -out $certificates/grafana.csr -keyout $certificates/grafanakey.pem -outform PEM > /dev/null
  openssl ca -config $config/openssl-ca.cnf -policy signing_policy -extensions signing_req -batch -cert $certificates/cacert.pem -keyfile $certificates/cakey.pem  -out $certificates/grafanacert.pem -infiles $certificates/grafana.csr
  rm -f $certificates/grafana.csr

  #link
  pushd $certificates > /dev/null
  ln grafanacert.pem $deploy/grafanacert.pem
  ln grafanakey.pem $deploy/grafanakey.pem
  echo "Cert and key created. "
;;
  "flux") 
  deploy=$project_base/provision/influx/https
  #remove old
  rm -f $certificates/influxcert.pem
  rm -f $certificates/influxkey.pem
  rm -f $deploy/influxcert.pem
  rm -f $deploy/influxkey.pem
  
  #generate new
  yes "" | openssl req -config $config/influx.cnf -newkey rsa:4096 -sha1 -nodes -out $certificates/influx.csr -keyout $certificates/influxkey.pem -outform PEM > /dev/null
  openssl ca -config $config/openssl-ca.cnf -policy signing_policy -extensions signing_req -batch -cert $certificates/cacert.pem -keyfile $certificates/cakey.pem  -out $certificates/influxcert.pem -infiles $certificates/influx.csr
  rm -f $certificates/influx.csr

  #link
  pushd $certificates > /dev/null
  ln influxcert.pem $deploy/influxcert.pem
  ln influxkey.pem $deploy/influxkey.pem
  echo "Cert and key created. "
;;
  "chron")
  deploy=$project_base/provision/chronograf/https
  #remove old
  rm -f $certificates/chronografcert.pem
  rm -f $certificates/chronografkey.pem
  rm -f $deploy/chronografcert.pem
  rm -f $deploy/chronografkey.pem
  
  #generate new
  yes "" | openssl req -config $config/chronograf.cnf -newkey rsa:4096 -sha1 -nodes -out $certificates/chronograf.csr -keyout $certificates/chronografkey.pem -outform PEM > /dev/null
  openssl ca -config $config/openssl-ca.cnf -policy signing_policy -extensions signing_req -batch -cert $certificates/cacert.pem -keyfile $certificates/cakey.pem  -out $certificates/chronografcert.pem -infiles $certificates/chronograf.csr
  rm -f $certificates/chronograf.csr

  #link
  pushd $certificates > /dev/null
  ln chronografcert.pem $deploy/chronografcert.pem
  ln chronografkey.pem $deploy/chronografkey.pem
  echo "Cert and key created. "
;;
  *)
  echo "Please enter \"./scripts/openssl_node.sh (flux | graf | chron)\""
;;
esac
