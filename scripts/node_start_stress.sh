#!/bin/bash

# need to be set
machines=10



counter=1
while [ $counter -le $machines ]
do
    echo "vagrant ssh node-$counter. Restart stress"
    vagrant ssh node-$counter -- 'sudo systemctl restart stress' 
    if ! [ "$?" -eq "0" ];then
        exit 
    fi
    ((counter++))
done    