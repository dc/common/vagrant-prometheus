#!/bin/bash

# need to be set
machines=10



counter=1
while [ $counter -le $machines ]
do
    echo "vagrant ssh node-$counter. Stop Stress."
    vagrant ssh node-$counter -- 'sudo systemctl stop stress' 
    if ! [ "$?" -eq "0" ];then
        exit 
    fi
    ((counter++))
done