# -*- mode: ruby -*-
# vi: set ft=ruby :

# Variables
BOX = "generic/arch"
BOX_NODE = "debian/contrib-stretch64"
BOX_NODE_V = "9.9.1"
# Provision folder
FOLDER = "/home/vagrant/fileprovision"
# 
CHRONOGRAF_GIT_URL = "https://aur.archlinux.org/chronograf-bin.git"

Vagrant.configure("2") do |config|
  #influx
  config.vm.define "flux", primary: true do |flux|
    flux.vm.box = "#{BOX}"

    # Network
    flux.vm.network "private_network",  ip: "10.0.0.98", auto_config: true
    # influxdb port (API / RPC)
    #flux.vm.network "forwarded_port", guest: 8086, host: 8086, host_ip: "127.0.0.1"
    #flux.vm.network "forwarded_port", guest: 8088, host: 8088, host_ip: "127.0.0.1"

    # Provision
    flux.vm.provision "file", source: "provision/influx", destination: "#{FOLDER}"

    flux.vm.provision "shell", inline: "rm -r -f /etc/pacman.d/gnupg"
    flux.vm.provision "shell", inline: "pacman-key --init"
    flux.vm.provision "shell", inline: "pacman-key --populate"
    
    flux.vm.provision "shell", inline: "timedatectl set-timezone Europe/Berlin"
    flux.vm.provision "shell", inline: "pacman -Syu --noconfirm"
    flux.vm.provision "shell", inline: "pacman -S --noconfirm influxdb"
    flux.vm.provision "shell", inline: "cp #{FOLDER}/config/influxdb.conf /etc/influxdb/influxdb.conf && chown influxdb:influxdb /etc/influxdb/influxdb.conf && chmod 440 /etc/influxdb/influxdb.conf"
    flux.vm.provision "shell", inline: "mkdir -p /var/lib/influxdb/https && cp -a #{FOLDER}/https/* /var/lib/influxdb/https && chown -R influxdb:influxdb /var/lib/influxdb/https && chmod 440 /var/lib/influxdb/https/* && chmod 550 /var/lib/influxdb/https"
    flux.vm.provision "shell", inline: "systemctl enable influxdb && systemctl start influxdb"
    flux.vm.provision "shell", path: "provision/influx/setup_influx.sh"
    flux.vm.provision "shell", inline: "mkdir -p /var/lib/influxdb/backup && cp -a #{FOLDER}/backup/* /var/lib/influxdb/backup"
    flux.vm.provision "shell", inline: "chown -R influxdb:influxdb /var/lib/influxdb/backup && chmod 550 /var/lib/influxdb/backup/* && chmod 550 /var/lib/influxdb/backup"
    flux.vm.provision "shell", inline: "cp -a #{FOLDER}/config/backup.* /etc/systemd/system/"
    flux.vm.provision "shell", inline: "systemctl enable backup.timer && systemctl start backup.timer"
    flux.vm.provision "shell", inline: "systemctl list-timers"
    
    flux.vm.provision "shell", inline: "pacman -S --noconfirm prometheus-node-exporter"
    flux.vm.provision "shell", inline: "systemctl enable prometheus-node-exporter && systemctl start prometheus-node-exporter"
    flux.vm.provision "shell", inline: "echo \"InfluxDB setup done. \""
    flux.vm.provision "shell", inline: "rm -r -f #{FOLDER}"

    # disable default sync folder
    flux.vm.synced_folder ".", "/vagrant", disabled: true
    # enable backup folder
    flux.vm.synced_folder "backup", "/vagrant/backup"
  end


  # prometheus 
  config.vm.define "prom" do |prom|
    prom.vm.box = "#{BOX}"
    
    # Network
    prom.vm.network "private_network",  ip: "10.0.0.100", auto_config: true
    prom.vm.network "forwarded_port", guest: 9090, host: 9090, host_ip: "127.0.0.1" 

    # Provision
    prom.vm.provision "file", source: "provision/prometheus", destination: "#{FOLDER}"

    prom.vm.provision "shell", inline: "rm -r -f /etc/pacman.d/gnupg"
    prom.vm.provision "shell", inline: "pacman-key --init"
    prom.vm.provision "shell", inline: "pacman-key --populate"

    prom.vm.provision "shell", inline: "timedatectl set-timezone Europe/Berlin"
    prom.vm.provision "shell", inline: "pacman -Syu --noconfirm"
    prom.vm.provision "shell", inline: "pacman -S --noconfirm prometheus"
    prom.vm.provision "shell", inline: "cp #{FOLDER}/config/prom.service /etc/systemd/system/prometheus.service"
    prom.vm.provision "shell", inline: "cp #{FOLDER}/config/prom.yml /etc/prometheus/prometheus.yml && chown prometheus:prometheus /etc/prometheus/prometheus.yml && chmod 440 /etc/prometheus/prometheus.yml"
    prom.vm.provision "shell", inline: "mkdir -p /var/lib/prometheus/https && cp -a #{FOLDER}/https/* /var/lib/prometheus/https  && chown -R prometheus:prometheus /var/lib/prometheus/https && chmod 440 /var/lib/prometheus/https/* && chmod 550 /var/lib/prometheus/https"
    prom.vm.provision "shell", inline: "systemctl daemon-reload && systemctl enable prometheus && systemctl start prometheus"
    
    prom.vm.provision "shell", inline: "pacman -S --noconfirm prometheus-node-exporter"
    prom.vm.provision "shell", inline: "systemctl enable prometheus-node-exporter && systemctl start prometheus-node-exporter"
    prom.vm.provision "shell", inline: "echo \"Prometheus setup done. \""
    prom.vm.provision "shell", inline: "rm -r -f #{FOLDER}"

    # Disable default sync folder
    prom.vm.synced_folder ".", "/vagrant", disabled: true
  end


  # grafana
  config.vm.define "graf" do |graf|
    graf.vm.box = "#{BOX}"

    # Network
    graf.vm.network "private_network",  ip: "10.0.0.99", auto_config: true
    graf.vm.network "forwarded_port", guest: 3000, host: 3000, host_ip: "127.0.0.1"

    # Provision
    graf.vm.provision "file", source: "provision/grafana", destination: "#{FOLDER}"

    graf.vm.provision "shell", inline: "rm -r -f /etc/pacman.d/gnupg"
    graf.vm.provision "shell", inline: "pacman-key --init"
    graf.vm.provision "shell", inline: "pacman-key --populate"

    graf.vm.provision "shell", inline: "timedatectl set-timezone Europe/Berlin"
    graf.vm.provision "shell", inline: "pacman -Syu --noconfirm"
    graf.vm.provision "shell", inline: "pacman -S --noconfirm grafana"
    graf.vm.provision "shell", inline: "pacman -S --noconfirm jq"
    graf.vm.provision "shell", inline: "cp #{FOLDER}/config/graf_conf.ini /etc/grafana.ini && chown grafana:grafana /etc/grafana.ini && chmod 440 /etc/grafana.ini"
    graf.vm.provision "shell", inline: "mkdir -p /var/lib/grafana/https && cp -a #{FOLDER}/https/* /var/lib/grafana/https && chown -R grafana:grafana /var/lib/grafana/https && chmod 440 /var/lib/grafana/https/* && chmod 550 /var/lib/grafana/https"
    graf.vm.provision "shell", inline: "systemctl enable grafana && systemctl start grafana"
    # grafana setup
    graf.vm.provision "shell", path: "provision/grafana/setup_grafana.sh"
    graf.vm.provision "shell", inline: "pacman -S --noconfirm prometheus-node-exporter"
    graf.vm.provision "shell", inline: "systemctl enable prometheus-node-exporter && systemctl start prometheus-node-exporter"
    graf.vm.provision "shell", inline: "echo \"Grafana setup done. \""
    graf.vm.provision "shell", inline: "rm -r -f #{FOLDER}"

    # disable default sync folder
    graf.vm.synced_folder ".", "/vagrant", disabled: true
  end


  #chronograf
  config.vm.define "chron" do |chron|
    chron.vm.box = "#{BOX}"

    # Network
    chron.vm.network "private_network",  ip: "10.0.0.97", auto_config: true
    chron.vm.network "forwarded_port", guest: 8888, host: 8888, host_ip: "127.0.0.1"

    # Provision
    chron.vm.provision "file", source: "provision/chronograf", destination: "#{FOLDER}"

    chron.vm.provision "shell", inline: "rm -r -f /etc/pacman.d/gnupg"
    chron.vm.provision "shell", inline: "pacman-key --init"
    chron.vm.provision "shell", inline: "pacman-key --populate"

    chron.vm.provision "shell", inline: "timedatectl set-timezone Europe/Berlin"
    chron.vm.provision "shell", inline: "pacman -Syu --noconfirm"
    chron.vm.provision "shell", inline: "pacman -S --noconfirm base-devel"
    chron.vm.provision "shell", inline: "pacman -S --noconfirm git"
    chron.vm.provision "shell", inline: "rm -r -f /home/vagrant/chron_git"
    chron.vm.provision "shell", inline: "git clone #{CHRONOGRAF_GIT_URL} /home/vagrant/chron_git"
    chron.vm.provision "shell", inline: "chown -R vagrant:vagrant /home/vagrant/chron_git"
    
    chron.vm.provision "shell", inline: "su - vagrant -c \"cd /home/vagrant/chron_git && makepkg -s -i -f --noconfirm --needed\""
    chron.vm.provision "shell", inline: "cp #{FOLDER}/config/chronograf.service /etc/systemd/system/chronograf.service"
    chron.vm.provision "shell", inline: "mkdir -p /var/lib/chronograf/https && cp -a #{FOLDER}/https/* /var/lib/chronograf/https && chown -R chronograf:chronograf /var/lib/chronograf/https && chmod 440 /var/lib/chronograf/https/* && chmod 550 /var/lib/chronograf/https"
    chron.vm.provision "shell", inline: "systemctl enable chronograf && systemctl start chronograf"

    chron.vm.provision "shell", inline: "pacman -S --noconfirm prometheus-node-exporter"
    chron.vm.provision "shell", inline: "systemctl enable prometheus-node-exporter && systemctl start prometheus-node-exporter"
    chron.vm.provision "shell", inline: "echo \"Chronograf setup done. \""
    chron.vm.provision "shell", inline: "rm -r -f #{FOLDER}"

    # disable default sync folder
    chron.vm.synced_folder ".", "/vagrant", disabled: true
  end


  # nodes 
  (1..10).each do |i|
    config.vm.define "node-#{i}", autostart: false do |node|
      node.vm.box = "#{BOX_NODE}"
      node.vm.box_version = "#{BOX_NODE_V}"
      
      # Network
      node.vm.network "private_network",  ip: "10.0.0.#{100+i}", auto_config: true
      
      # Provision
      node.vm.provision "file", source: "provision/node", destination: "#{FOLDER}"

      node.vm.provision "shell", inline: "timedatectl set-timezone Europe/Berlin"
      node.vm.provision "shell", inline: "apt-get update && apt-get upgrade -y"
      node.vm.provision "shell", inline: "apt-get install -y prometheus-node-exporter"
      node.vm.provision "shell", inline: "systemctl enable prometheus-node-exporter && systemctl start prometheus-node-exporter"
      node.vm.provision "shell", inline: "cp #{FOLDER}/config/stress.service /etc/systemd/system/stress.service"
      node.vm.provision "shell", inline: "systemctl enable stress"
      node.vm.provision "shell", inline: "echo \"Node setup done. \""
      node.vm.provision "shell", inline: "rm -r -f #{FOLDER}"

      # disable default sync folder
      node.vm.synced_folder ".", "/vagrant", disabled: true
    end
  end
end