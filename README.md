# vagrant-prometheus

## Einleitung

Mithilfe dieses Workflows können Sie mit Vagrant eine vollständige Monitoring-Infrastruktur aus den folgenden Komponenten erstellen. Jede der Komponenten wird auf einer eigenen virtuellen Maschine durch ein idempotentes Skript installiert. 

* Grafana (Visualisierung, Graphen, Dashboards)
* Prometheus (Node-Metriken abgreifen und an DB weiterleiten)
* InfluxDB (Time-Series DB, Langzeitspeicher, Backup/Restore)
* Chronograf (Graphisches Interface zu InfluxDB)
* Node-Set (Dummy-Maschinen)

Für das Bereitstellen der Metriken, ist auf jeder virtuellem Maschine (VM) der Prometheus-node-exporter installiert. 

<img src="./docu/setup.png"  width="800">

## Voraussetzungen

Um den Workflow ausführen zu können müssen folgende Programme installiert sein:

* Vagrant (<https://www.vagrantup.com/>)
* VirtualBox (<https://www.virtualbox.org/>) oder ein anderer unterstützter Hypervisor
* Git (<https://git-scm.com/>)

Im Weiteren wird vorausgesetzt, dass Sie sich auf einem Linux-basierten Betriebssystem befinden. Der Workflow funktioniert auch auf Windows-Platformen, hierbei ist aber zu beachten, dass unter Windows standardmäßig Zeilenumbrüche mit CR & LF kodiert werden, im Gegensatz zum einfachen LF Zeilenumbruch unter Linux. Da die Dateien mit dem File-Provisionierer übertragen werden, muss darauf geachtet werden, dass die Dateien mit einem einfachen LF gespeichert werden. In der Git-Konfiguration sollte demensprechend folgende Funktion abgeschaltet werden. Diese ist normalerweise unter Windows eingeschaltet und fügt beim auschecken ein CR zum LF hinzu.

```shell
git config --global core.autocrlf false
```

## Getting started

Um direkt zu starten, kopieren Sie mit folgendem Befehl das Projekt auf ihren Rechner und wechseln anschließend in das erstellte Verzeichnis.

```shell
git clone git@git.gsi.de:dc/common/vagrant-prometheus.git
```

Bevor die VMs gestartet werden, muss eine CA und durch sie signierte TLS-Zertifikate für Influx, Grafana und Chronograf erzeugt werden. Hierfür führen Sie folgenden Befehl aus.

```shell
./scripts/openssl_generate.sh
```

Nachdem die Zertifikate erzeugt wurden, können die VMs gestartet werden.

```shell
vagrant up
```

Anschließen starten nacheinander Influx, Prometheus, Grafana und Chronograf. Die Nodes, dessen Metriken erfasst werden sollen, müssen explizit gestartet werden, entweder einzeln oder als Regex.

```shell
vagrant up {node-1 node-2 ... node-10 | /node-.*/}
```  

## Funktionsprüfung

Nachdem die Maschinen gestartet sind und die Provisionierung erfolgreich ausgeführt wurde, können wir über die von Vagrant weitergeleiteten Ports direkt auf die Dienste zugreifen.

* <https://127.0.0.1:3000> (Grafana)
* <https://127.0.0.1:8888> (Chronograf)

Wenn auf Ihrem Betriebssystem diese Ports schon in Verwendung sind, können Sie in der Vagrantfile eine andere Zuordnung konfigurieren.

Um Zertifikat-Warnungen zu vermeiden, können Sie das CA-Zertifikat ```./openssl/data/certificates/cacert.pem``` als vertrauenswürdig in Ihrem Browser eintragen. Wenn Sie das Zertifikat hinzufügen, sollten Sie sicherstellen, dass die durch den Browser angezeigten Fingerprints denen der Ausgabe des folgenden Befehls entsprechen.

```shell
openssl x509 -noout -fingerprint -sha256 -inform pem -in cacert.pem
openssl x509 -noout -fingerprint -sha1 -inform pem -in cacert.pem
```

## Vagrant Kommandos

* ```vagrant up <vm>```
    Startet und erstellt, falls nötig, die Maschine. Wird die Maschine zum ersten Mal gestartet, wird auch die Provisionierung ausgeführt, andernfalls muss mit der Option "--provision" die Provisionierung explizit gefordert werden.

* ```vagrant destroy <vm>```
    Löscht die Maschine.

* ```vagrant provision <vm>```
    Mit diesem Befehl führen Sie bei einer laufenden Maschine die Provisionierung neu aus. Dabei werden die Befehle aus der Vagrantfile (%vm_name%.vm.provision) in der dort angegebenen  Reihenfolge nacheinander ausgeführt.

* ```vagrant ssh <vm>```
    Baut eine SSH-Verbindung zu der angegebenen VM auf.

## Klassifizierung von Dateien

Um eine überschaubare Umgebung zu schaffen, werden die hier verwendeten Dateien in drei Kategorien zu unterteilt.

### 1. Konfigurations-Daten

Konfigurationsdateien enthalten von Benutzer angegebene Parameter für den Betrieb einer Software gebraucht werden. Hierrunter fallen z.B. Ini- oder Conf-Dateien.

Einen Sonderfall stellen in diesem Projekt die Text- und JSON-Dateien dar, Diese enthalten Daten, die nicht direkt von den Executables interpretiert werden, sondern z.B. die JSON-Dateien mittels CURL an die HTTP-API von Grafana gesendet werden, um einen Zustand zu erzeugen. Da der Workflow für Openssl bzw. Zertifikate in einer anderen Sektion beschrieben wird, übergehen wir hier diese Dateien.

```shell
.vagrant-prometheus/
    ./openssl
        ./config/*.cnf                  # Zertifikat Konfigurationen
    ./provision
        ./grafana
            ./config
                ./dashboards/*.json     # Grfana Dashboards
                ./datasources/*.json    # Grafana Datasources
                ./users/*.json          # Grafana Benutzer
                ./graf_conf.ini         # Konfigurationsdatei
                ./request_token.json    # API-Token
        ./influx
            ./config
                ./influxdb.conf         # Konfigurationsdatei
                ./setup.queries         # Queries die auf der Influxdb ausgeführt werden müssen
            ./setup_influx.sh           # Sonderfall, da hier die Standardwerte für den Admin-Benutzer stehen
        ./prometheus
            ./config
                ./prom.yml              # Konfigurationsdatei
```

### 2. Benutzer-Daten

Hiermit sind alle Daten gemeint, die während des Betriebs entstehen. In diesem Projekt sind dies vorwiegend die von den Exportern erhobenen Daten in der Influx-Datenbank.

* Chronograf

    ```shell
    /var/lib/chronograf/chronograf-v1.db
    ```

* Grafana

    ```shell
    /var/lib/grafana/
        ./grafana.db
        ./token/apitoken.json
    ```

* Influxdb

    ```shell
    /var/lib/influxdb/
        ./data
        ./meta
        ./wal
    ```

* Prometheus

    ```shell
    /var/lib/prometheus/data
    ```

### 3. Executable

In die Kategorie Executable fallen alle Dateien, die weder Konfig- noch Bentuzer-Daten sind.

```shell
.vagrant-prometheus/
    ./provision
        ./chronograf
            ./config
                ./chronograf.service
        ./grafana  
            ./setup_grafana.sh
        ./influx
            ./backup
                ./backup_call.sh
                ./backup.sh
                ./restore.sh
            ./config
                ./backup.service
                ./backup.timer
            ./setup_influx.sh
        ./node
            ./config
                ./stress.service
        ./prometheus
            ./config
                ./prom.service
    ./scripts
        ./influx_backup.sh
        ./influx_restore.sh
        ./node_start_stress.sh
        ./node_stop_stress.sh
        ./openssl_fingerprint.sh
        ./openssl_generate.sh
        ./openssl_node.sh
```

## Backup- und Restore-Workflow

### Backup

Die InfluxDB ist durch einen Backup- und Restore-Mechanismus ausgestattet, mit dem wir die gesammelten Daten des Monitoringsystems exportiern und wiederherstellen können. In diesem Workflow wird standardmäßig täglich um 1 Uhr ein Vollbackup ausgelöst, das separat zu den vorherigen Backups in einem Ordner mit Zeitstempel abgelegt wird. Konfigurieren können Sie den Timer in der Datei ```./provision/influx/config/backup.timer```.

Um ein manuelles Backup zu starten, führen Sie folgendes Skript aus.

```shell
./scripts/influx_backup.sh %PFAD%
```

Mit der %PFAD% Variable geben Sie den Ausgabepfad des Backups an. Dabei beschreibt der Pfad den Speicherort innerhalb der VM.

### Restore

Um ein Backup in die InfluxDB wiederherzustellen, benutzen Sie folgendes Skript.

```shell
./scripts/influx_restore.sh <PFAD>
```

Die %PFAD% Variable ist hierbei der Ordner indem sich die Backupdateien befinden. Sollte in Ihrer Influx-Instanz schon die Datenbank "prom" existieren, werden Sie gefragt ob Sie diese löschen wollen, um die "prom"-Datenbank aus dem Backup laden zu können. Diese Operation ist notwendig, da hier bei jedem Backup die vollständige Datenbank gesichert wird und dementsprechend eine vollständige Datenbank wiederhergestellt wird.

### Backupmechanismus überprüfen

TODO

## Passwort-Änderungs-Workflows

### Grafana

Für Grafana werden zwei Benutzer angelegt, ein Administrator und ein optionaler View-Benutzer mit der Berechtigung, Dashboards anzuzeigen. Angelegt werden die Benutzer mit dem Skript ```./provision/grafana/setup_grafana.sh```. Die Standardpasswörter für die beiden Benutzer können in folgenden JSON-Dateien bearbeiten werden. Nachdem die Benutzer erzeugt wurden, wird zusätzlich ein API-Token erstellt, mit dem anschließend alle Änderungen gemacht werden. Gespeichert wird dieser Token innerhalb der VM im Verzeichnis ```/var/lib/grafana/token```. Anhand der Existenz dieses Tokens innerhalb der VM entscheidet das Skript, ob Grafana initial konfiguriert werden muss.

```shell
./provision/grafana/config/users
    ./admin_password.json
    ./view_user.json
```

#### Passwörter ändern

Das Ändern von Benutzer Passwörter sollte nach Möglichkeit über das Grafana-UI erfolgen, entweder als Admin oder vom Benutzer selbst. Daneben besteht noch die Option, die HTTP-API zu benutzen. Hierfür können Sie sich am ```setup_grafana.sh```-Skript von Grafana orientieren und den CURL Befehl entweder mit BasicAuth oder mit dem API-Token bauen.

#### Admin Passwort zurücksetzen

Sollte kein Zugriff auf den Admin-Benutzer bestehen, kann mit der Grafana-CLI das Passwort zurückgesetzten werden. Hierfür muss lokal auf der VM folgender Befehl ausgeführt werden.

```shell
vagrant ssh graf    # verbinden mit der VM 
sudo grafana-cli --homepath /usr/share/grafana admin reset-admin-password "newpass"
```

### Influx

In der Influx-Datenbank werden drei Benutzer angelegt, ein Administrator und anschließend zwei weitere Benutzer für Grafana und Prometheus. Dabei ist festgelegt, dass der Prometheus-Benutzer nur Schreib-Rechte und Grafana nur Lese-Rechte bekommt. Ausgeführt werden die Queries mit dem Skript ```./provision/influx/setup_influx.sh``` indem mit CURL die Queries an die HTTP-API von Influx gesendet werden. Beim ersten Start von Influxdb wird keine Authentifizierung für das Erzeugen eines Administrators verlangt. Anschließend schaltet sich die Authentifizierung automatisch für die nachfolgenden Queries ein. Die Standardpasswörter für die Benutzer sind in folgenden Dateien gespeichert.

```shell
./provision/influx/
    ./setup_influx.sh       # Admin-Benutzer
    ./config/setup.queries  # Prometheus- und Grafana-Benutzer
```

#### Passwörter ändern

Passwörter können Sie nur als Admin-Benutzer ändern. Die einfachste Möglichkeit wäre es, im UI von Chronograf in der Sektion 'InfluxDB Admin' diese zu ändern. Die andere Möglichkeit wäre, sich mit dem Influx-CLI Tool  auf der Influx-Datenbank anzumelden. Da das Tool die HTTP-API benutzt, kann auch von einer anderen VM die Anmeldung erfolgen. Für den Moment ist nur auf der flux-VM das Tool installiert, darum müssen wir und mit der flux-VM verbinden.

```shell
vagrant ssh flux
influx -ssl -unsafeSsl
>auth  
username:
password:
>SET PASSWORD FOR "name" = 'newpass'
result message...
```

Wenn Sie das Passwort von Prometheus oder Grafana ändern, müssen Sie auch die Konfiguration bzw. die Grafana-Datasource aktualisieren.

##### Prometheus
Tragen Sie die neuen Benutzerdaten in folgende Datei ein, führen Sie die Provisionierung neu aus und laden Sie die Konfiguration neu.

```shell
nano ./provision/prometheus/config/prom.yml

vagrant provision prom  
vagrant ssh prom
sudo systemctl reload prometheus
```

##### Grafana
Tragen Sie die neuen Benutzerdaten in folgende Datei ein, führen Sie die Provisionierung neu aus.

```shell 
./provision/grafana/config/datasources/ds_influx.json
```

#### Influx-Admin Passwort zurücksetzen

Sollte ein 2.ter Admin-Benutzer existieren, kann mit den oben beschriebenen Methoden das Passwort geändert werden. Ist kein weitere Admin-Benutzer verfügbar, muss die Authentifizierung abgeschaltet werden und dann ein neuer Admin erzeugt werden.

```shell 
vagrant ssh flux
# Ändern sie in der Sektion [http], die Variable "auth-enabled" = false
sudo nano /etc/influxdb/influxdb.conf
# influx neustarten
systemctl restart influx
# influx verbinden
influx -ssl -unsafeSsl 
>SET PASSWORD FOR "name" = 'newpass'
>exit
# Ändern sie in der Sektion [http], die Variable "auth-enabled" = true
sudo nano /etc/influxdb/influxdb.conf
# influx neustarten
systemctl restart influx
```

## Zertifikat-Änderungs-Workflows

Um den Datenverkehr zu verschlüsseln und die Dienste authentifizieren zu können, benötigen wir TLS-Zertifikat die wir mit dem Openssl-Toolkit erzeugen (<https://www.openssl.org/>). Hierfür wird zuerst ein Root-Zertifikat (cacert.pem) erzeugt und anschließend die Zertifikate für die Dienste Grafana, Influx und Chronograf. Diese werden von dem Root-Zertifikat signiert, sodass mit dem Root-Zertifikat die Authentifizierung erfolgen kann. Wichtig ist zu beachten, dass zu jedem Zertifikat auch ein Private-Key existiert, so z.B. cacert.pem und cakey.pem oder influxcert.pem und influxkey.pem.

### Openssl Ordnerstruktur

```shell
./openssl
    ./config
        ./chronograf.cnf
        ./grafana.cnf
        ./influx.cnf
        ./openssl-ca.cnf
    ./data
        ./certificates
        ./csrdb
```

Im Config-Verzeichnis befinden sich die Konfigurationsdateien für die Zertifikate. In diesen können Sie die Standardwerte für organizationName, commonName, emailAddress, usw. ändern, die bei der Erzeugung in die Zertifikate eingetragen werden. Das Data-Verzeichnis wird erst nachdem ausführen von ```./scripts/openssl_generate.sh```-Skript erstellt und enthält die Zertifikate im Ordner "certificates", sowie den Zustand der Openssl-Textdatenbank im Ordner "csrdb". Csrdb wird für das Signieren der Zertifikate gebraucht und enthält unteranderem die aktuelle Seriennummer.

### Zertifikat-Seriennummer

Zertifikate enthalten nach dem X.509 Standard eine Seriennummer, die bei der Signierung bestimmt wird. Die mit dem ```./scripts/openssl_generate.sh``` erstellten und signierten Zertifikate werden standardmäßig mit der Seriennummer 0x2 aufwärts signiert. Manche Browser, wie z.B. der Firefox, melden aber einen Fehler, wenn vom Selben "Issuer: C, ST, L, O, OU, CN" eine vergebene Seriennummer nochmal an ein anderes Zertifikat vergeben wird. In diesem Fall können sie mit einem CLI-Argument die Anfangsseriennummer auf einen andere Wert setzen. Die Seriennummer wird als Hex-Zahl angegeben und die Anzahl der Ziffern muss gerade sein. So würde z.B. 0F1 zu einem Fehler führen und sollte als F1 oder 00F1 eingegeben werden.

```shell
#./scripts/openssl_generate.sh 01
#./scripts/openssl_generate.sh FF
#./scripts/openssl_generate.sh 0100
#./scripts/openssl_generate.sh FFFF
#./scripts/openssl_generate.sh 010000
#./scripts/openssl_generate.sh 0FFF8729  
#./scripts/openssl_generate.sh 01FA55DEC3
...
```

### Private-Key verloren

Sollten Sie ihren CA-Key verlieren, kann ein Angreifer eigene Services aufbauen und wir würden diesen vertrauen.

Abhilfe schafft das Erzeugen einer neuen CA, der Entzug des Vertrauens der alten CA und das Vertrauen der neuen CA. Anschließend müssen sämtliche Dienst-Zertifikate erneuert und von der neuen CA signiert werden. Falls die neue CA denselben Issuer (C, ST, L, O, OU, CN) hat, müssen die Dienst-Zertifikate mit einer höheren Seriennummer erzeugt werden.

Falls der Key eines Dienstes verlorengeht, könnte ein Angreifer sich als diesen Dienst ausgeben. 

In diesem Fall müssen wir ebenso alle Zertifikate erneuen, weil wir keinen Zertifikat-Revocation-Mechanismus implementiert haben. 


Nachdem Zertifikate ausgetauscht wurden, müssen die entsprechenden Dienste neugestartet werden.

Hierfür bitte folgendes Ausführen. Beachten Sie die Zertifikatsseriennummmer.
```shell
./scripts/openssl_generate.sh $SN
vagrant provision --all
```

### Eigene Zertifikate

Wenn Sie eigene Zertifikate erzeugen und diese in diesem Workflow benutzen möchten, müssen Sie diese an die entsprechenden Pfade kopieren oder verlinken. Standardmäßig werden die erzeugten Zertifikate aus ```./openssl/data/certificates``` mit dem jeweiligen Provision-Verzeichnis der VMs verlinkt. Sie können an dieselben Stellen ```./provision/*/https``` Ihre Zertifikate kopieren und mit dem Provisionierer diese übertragen lassen. Sollten ihre Zertifikate nicht wie unten benannt werden können, müssen Sie zusätzlich in den Konfigurationen die geänderten Dateinamen eintragen.

```shell
./provision
    ./chronograf
        ./config/chronograf.service # TLS_CERTIFICATE, TLS_PRIVATE_KEY
        ./https
            ./cacert.pem
            ./chronografcert.pem
            ./chronografkey.pem
    ./grafana
        ./config/graf_conf.ini      # Sektion [server]: cert_file, cert_key
        ./https
            ./cacert.pem
            ./grafanacert.pem
            ./grafanakey.pem
    ./influx
        ./config/influxdb.conf      # Sektion [http]: https-certificate, https-private-key
        ./https
            ./cacert.pem
            ./influxcert.pem
            ./influxkey.pem
```
