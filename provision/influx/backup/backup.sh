#!/bin/bash



echo "Running: $(pwd)/backup.sh"
# run as root
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit
fi


# default backup path
if [ "$1" == "" ];then
	# default incremental backup
	backup_path="/vagrant/backup/default_inc"
else
	# backup to desired location
	backup_path="$1"
fi


lastbackup="$(cat $backup_path/lastbackup)"
if [ "$?" -eq 0 ];then
	# increment
	echo "Increment"
	echo "$(date --rfc-3339=seconds -u | sed 's/ /T/' | sed 's/+.*/Z/')" > $backup_path/lastbackup
	echo "Running: influxd backup -portable -host 127.0.0.1:8088 -db prom -since $lastbackup $backup_path "
	echo ""
	influxd backup -portable -host 127.0.0.1:8088 -db prom -since $lastbackup $backup_path 
	exit $?
else
	# create new one
	echo "Fullbackup"
	rm -r -f $backup_path
	mkdir -p $backup_path
	
	lastbackup="$(date --rfc-3339=seconds -u | sed ' s/ /T/' | sed 's/+.*/Z/')"
	echo "$lastbackup" > $backup_path/lastbackup
	echo "Running: influxd backup -portable -host 127.0.0.1:8088 -db prom $backup_path "
	echo ""
	influxd backup -portable -host 127.0.0.1:8088 -db prom $backup_path 
	exit $?
fi
