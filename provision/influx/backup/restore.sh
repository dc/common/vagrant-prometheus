#!/bin/bash



# run as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


influx="/var/lib/influxdb"
cacert=$influx/https/cacert.pem
backup_path=""


# credentials
echo "Please enter the Admin-Credentials in format user:password. " 
read credentials


# check program args
if [ "$1" == "" ];then
  #backup_path="/vagrant/backup/default_inc"
  echo "Please specify the path to your backupfiles. "
  exit
else
  backup_path="$1"
fi


# check if exist
if ! [ -d $backup_path ]
then
  echo "Please create first a backup. "
  exit
fi


# Check if database already exist, since influxdb cannot insert into existing DB.
result="$(curl -XPOST https://localhost:8086/query --cacert $cacert -u $credentials --data "q=SHOW DATABASES" -H "Content-Type: application/x-www-form-urlencoded" 2> /dev/null)"
if [[ $result =~ "prom" ]]
then
  echo "Database prom exist. "
  echo -n "Do you want delete 'prom' to restore another state ? [y/n]: " 
  read prompt
  if [ "$prompt" != "${prompt#[Yy]}" ] ;then
    echo -n "curl: Query delete database: "
    curl -XPOST https://localhost:8086/query --cacert $cacert -u $credentials --data "q=DROP DATABASE prom" -H "Content-Type: application/x-www-form-urlencoded" 2> /dev/null
  else
    exit
  fi
fi
        
        
echo "Running: influxd restore -portable -host 127.0.0.1:8088 -db prom $backup_path"
echo ""
influxd restore -portable -host 127.0.0.1:8088 -db prom $backup_path
exitcode=$?
if  [  "$exitcode" -eq "0" ]; then
    echo "Influxd restore successfull. "
    echo -n "curl: Query setting access rights: "
    curl -XPOST https://localhost:8086/query --cacert $cacert -u $credentials --data "q=GRANT WRITE ON prom TO prometheus" -H "Content-Type: application/x-www-form-urlencoded" 2> /dev/null
else
    echo "Influxd exited with $exitcode. "
    echo "Access rights for user prometheus need to be set manually. "
    exit 1
fi