#!/bin/bash



influx_backup=/var/lib/influxdb/backup
backup=/vagrant/backup


postfix="$(date --rfc-3339=seconds -u | sed ' s/ /T/' | sed 's/+.*/Z/')"


pushd $influx_backup
#./backup.sh
./backup.sh "$backup/backup_$postfix"

