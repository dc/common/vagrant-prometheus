#!/bin/bash



# waiting for database to be ready 
sleep 5

provision=/home/vagrant/fileprovision
cacertpath=/var/lib/influxdb/https/cacert.pem
queries="$provision/config/setup.queries"


# Here you can set the initial admin credentials 
name=admin
password=default

echo "Query: CREATE USER $name WITH PASSWORD '$password' WITH ALL PRIVILEGES"
curl -XPOST https://localhost:8086/query --cacert $cacertpath -u "$name:$password" --data "q=CREATE USER $name WITH PASSWORD '$password' WITH ALL PRIVILEGES" -H "Content-Type: application/x-www-form-urlencoded" 2> /dev/null
echo " "

# Execute 'setup.queries' 
while read LINE;
do
	if [[ $LINE == \q* ]] 
    then
      echo "Query: ${LINE:2}"
			curl -XPOST https://localhost:8086/query --cacert $cacertpath -u "$name:$password" --data "$LINE" -H "Content-Type: application/x-www-form-urlencoded" 2> /dev/null
			echo " "
    else
        continue    
  fi
done < $queries
echo "Setup queries done. "