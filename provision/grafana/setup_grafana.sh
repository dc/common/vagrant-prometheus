#!/bin/bash



tokenpath=/var/lib/grafana/token
provision=/home/vagrant/fileprovision
cacertpath=/var/lib/grafana/https/cacert.pem


# wait for grafana to be ready
sleep 5


# Create API Token if absent 
if ! [ -f $tokenpath/apitoken.json ];then
  mkdir $tokenpath
  curl -X POST -H "Content-Type: application/json" --data-binary "@$provision/config/request_token.json" https://admin:admin@localhost:3000/api/auth/keys --cacert $cacertpath > $tokenpath/apitoken.json 2> /dev/null
  echo "Grafana API Token created. "


  # add simple view user (Grafana support for the Admin Api only BasicAuth, therefore i create the view user bevor i switch to another password)
  curl -X POST -H "Content-Type: application/json" --data-binary "@$provision/config/users/view_user.json" https://admin:admin@localhost:3000/api/admin/users --cacert $cacertpath 2> /dev/null
  echo "Grafana viewuser created. "

  # change default password
  curl -X PUT -H "Content-Type: application/json" --data-binary "@$provision/config/users/admin_password.json" https://admin:admin@localhost:3000/api/user/password --cacert $cacertpath 2> /dev/null
fi

echo "API-Token is located at: $tokenpath/apitoken.json"
apitoken=$(jq '.key' $tokenpath/apitoken.json) 
# Remove quote's from string
temp="${apitoken%\"}"
apitoken="${temp#\"}"


# Datasource
# copy the key and remove \r (CR)
key="$(sed -e 's/\r//' $cacertpath)"
# set correct tlsCACert to verify signing
json="$(jq --arg value "$key" '.secureJsonData.tlsCACert = $value' $provision/config/datasources/ds_influx.json)"
curl -X DELETE https://localhost:3000/api/datasources/name/InfluxDB -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $apitoken" --cacert $cacertpath 2> /dev/null
curl -X POST https://localhost:3000/api/datasources -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $apitoken" --data "$json" --cacert $cacertpath 2> /dev/null
echo "Grafana datasources influx. "

# Dashboard
curl -X DELETE https://localhost:3000/api/dashboards/uid/default_dashboard_production -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $apitoken" --cacert $cacertpath 2> /dev/null
curl -X POST https://localhost:3000/api/dashboards/db -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $apitoken" --data-binary "@$provision/config/dashboards/dash_production.json" --cacert $cacertpath 2> /dev/null

curl -X DELETE https://localhost:3000/api/dashboards/uid/default_dashboard_monitoring -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $apitoken" --cacert $cacertpath 2> /dev/null
curl -X POST https://localhost:3000/api/dashboards/db -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $apitoken" --data-binary "@$provision/config/dashboards/dash_monitoring.json" --cacert $cacertpath 2> /dev/null
echo "Grafana dashboard. "